<?php 

    include ('connexionbdd.php');

    //va rechercher toutes les fiches d'un joueur en particulier, pour en récupérer quelques infos pour les 
    //afficher dans un index.
    
    $req = 'SELECT joueurs.login, classes.classe, guildes.nom_guilde, personnages.id, nom_perso, prenom_perso

            FROM personnages
    
            LEFT JOIN joueurs ON personnages.id_joueurs = joueurs.id 

            LEFT JOIN classes ON personnages.id_classes = classes.id
    
            LEFT JOIN guildes ON personnages.id_guildes = guildes.id
    
            WHERE joueurs.login = \''. $_SESSION['login'].'\' 
        
            ORDER BY nom_perso ';
    
    $dmd = $bdd->query($req);
    $reponse = $dmd->fetchAll(PDO::FETCH_ASSOC);

?>