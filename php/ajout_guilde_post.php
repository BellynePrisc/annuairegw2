<?php 

    if (isset($_POST['submit'])){

        //Connexion à la Bdd 
        include ('connexionbdd.php');

           error_reporting(-1);
             //on vérifie dans la BDD que la guilde n'est pas déjà présente
            $erreur_guilde = "";
            $guilde_ok = "";
            $upload_ok = "";
            $upload_fail = "";

            $reponse = $bdd->query('SELECT COUNT(*) AS `compteur` FROM `guildes`WHERE `nom_guilde`= "'.$_POST['nom_guilde'].'"');
            $donnee = $reponse->fetch();
            $compteur = $donnee['compteur'];
                if ($compteur > 0) {
                    $erreur_guilde = "Cette guilde a déjà été enregistrée.";
                } 
                else {    
                    if($_FILES['photo']['size']== 0) {
                        $upload_fail = "L'image n'a pas pu être téléchargée.";
                    }
                    else {
                    //on upload l'image       
                    $target_Path = "../uploads/";
                    $target_Path = $target_Path.basename( $_FILES['embleme_guilde']['name'] );
                    move_uploaded_file( $_FILES['embleme_guilde']['tmp_name'], $target_Path );
                    }
                    //on insère les données en BDD
                    $req = $bdd->prepare("INSERT INTO guildes (nom_guilde, embleme_guilde) VALUES (?, ?)");
                    $req->execute(array(
                        $_POST['nom_guilde'],
                        $_FILES['embleme_guilde']['name']
                    )); 
                }

            //on affiche le message de validation
            $upload_ok= "L'image à bien été uploadée !";
            $guilde_ok = "La Guilde a bien été créée !";

    }
?>