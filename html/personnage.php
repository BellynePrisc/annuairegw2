<?php 
session_start();
include('../php/connexionbdd.php');
include('../php/personnage_post.php');
include('../php/affiche_personnage_simple.php');
if (!empty($personnage)) header("Refresh: 3;url=accueil.php");
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/style.css" />
        <title>Nouvelle Fiche Personnage</title>
        <!-- début Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="../img/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../img/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../img/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../img/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../img/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../img/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../img/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../img/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../img/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="../img/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../img/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon-16x16.png">
        <link rel="manifest" href="../img/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- Fin Favicon -->
    </head>
    <body>
        
        <?php require 'header.php';
        
            
            
        ?>

        <main>
            <form id="monformulaire3" name="monformulaire3" <?php if ($_SESSION['id_fiche']) { echo 'action="affiche_personnage.php"';} else {echo 'action="personnage.php"';} ?> method="post" enctype="multipart/form-data">
                <div id="group1">
                    <fieldset id="bloc1">
                        <legend>Identité du Personnage</legend>

                        <label for="nom_perso">Nom
                        <input type="text" name="nom_perso" id="nom_perso" value="<?php 

                            if (isset($_POST['nom_perso'])){
                                echo $_POST['nom_perso'];
                            }
                            if ($_SESSION['id_fiche']) {
                                echo $resultat['nom_perso'];
                            } 
                            ?>" />
                        </label>   

                        <label for="prenom_perso">Prénom
                        <input type="text" name="prenom_perso" id="prenom_perso" value="<?php
                        if (isset($_POST['prenom_perso'])){
                            echo $_POST['prenom_perso'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['prenom_perso'];
                        } ?>" /></label>

                        <label for="age_perso">Âge
                        <input type="number" name="age_perso" id="age_perso" step="1" value="<?php 
                        if (isset($_POST['age_perso'])){
                            echo $_POST['age_perso'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['age_perso'];
                        }
                        ?>" /></label>

                        <label for="metier">Métier
                        <input type="text" name="metier" id="metier" value="<?php  
                        if (isset($_POST['metier'])){
                            echo $_POST['metier'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['metier'];
                        }
                        ?>" /></label>

                        <label for="classe">Classe
                            <select name="classe" size="1">
                                <option value="" selected></option>
                                <?php
                                    $resultat1=$bdd->query("SELECT * FROM classes");
                                    $resultat1->setFetchMode(PDO::FETCH_ASSOC);
                                        foreach ($resultat1 as $data){
                                        echo '<option value="' . $data['id'] . '">'. $data['classe']. '</option>';
                                        };
                                ?>
                            </select>
                        </label>

                        <label for="guilde">Guilde
                            <select name="guilde" size="1">
                                <option value="" selected></option>
                                <?php  
                                    $resultat2=$bdd->query("SELECT * FROM guildes");
                                    $resultat2->setFetchMode(PDO::FETCH_ASSOC);
                                        foreach ($resultat2 as $data){
                                        echo '<option value="' . $data['id'] . '">'. $data['nom_guilde']. '</option>';
                                        };
                                ?>
                            </select>
                        </label>
                                        
                        <label for="role_guilde">Emploi Guilde
                        <input type="text" name="role_guilde" id="role_guilde" value="<?php  
                        if (isset($_POST['role_guilde'])){
                            echo $_POST['role_guilde'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['role_guilde'];
                        }
                        ?>" /></label>

                        <label for="photo">Photo (pas d'espace ni ponctuation)
                        <input type="file" name="photo" id="photo" accept=".jpg, .jpeg, .png" value="" /></label>
                    </fieldset>

                    <fieldset id="bloc2">
                        <legend>Ses Caractéristiques</legend>
                        <label for="force_perso">Force
                        <input type="number" name="force_perso" id="force_perso" min="1" max="20" step="1" value="<?php
<<<<<<< HEAD
                        if (isset($_POST['force_perso'])) {
=======
                        if (isset($_POST['force_perso'])){
>>>>>>> origin/dev_kevin
                            echo $_POST['force_perso'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['force_perso'];
                        }
                        ?>" /></label> 

                        <label for="constitution_perso">Constitution
                        <input type="number" name="constitution_perso" id="constitution_perso" min="1" max="20" step="1" value="<?php
                        if (isset($_POST['constitution_perso'])) {
                            echo $_POST['constitution_perso'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['constitution_perso'];
                        }
                        ?>" /></label>

                        <label for="dexterite_perso">Dextérité
                        <input type="number" name="dexterite_perso" id="dexterite_perso" min="1" max="20" step="1" value="<?php
                        if (isset($_POST['dexterite_perso'])){
                            echo $_POST['dexterite_perso'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['dexterite_perso'];}
                        ?>" /></label>

                        <label for="intelligence_perso">Intelligence
                        <input type="number" name="intelligence_perso" id="intelligence_perso" min="1" max="20" step="1" value="<?php
                        if (isset($_POST['intelligence_perso'])){
                            echo $_POST['intelligence_perso'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['intelligence_perso'];}
                        ?>" /></label>

                        <label for="charisme_perso">Charisme
                        <input type="number" name="charisme_perso" id="charisme_perso" min="1" max="20" step="1" value="<?php
                        if (isset($_POST['charisme_perso'])){
                            echo $_POST['charisme_perso'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['charisme_perso'];}
                        ?>" /></label>  

                        <label for="perception_perso">Perception
                        <input type="number" name="perception_perso" id="perception_perso" min="1" max="20" step="1" value="<?php
                        if (isset($_POST['perception_perso'])){
                            echo $_POST['perception_perso'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['perception_perso'];}
                        ?>" /></label>

                        <label for="volonte_perso">Volonté
                        <input type="number" name="volonte_perso" id="volonte_perso" min="1" max="20" step="1" value="<?php
                        if (isset($_POST['volonte_perso'])){
                            echo $_POST['volonte_perso'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['perception_perso'];}
                        ?>" /></label>

                        <label for="rapidite_perso">Rapidité
                        <input type="number" name="rapidite_perso" id="rapidite_perso" min="1" max="20" step="1" value="<?php
                        if (isset($_POST['rapidite_perso'])){
                            echo $_POST['rapidite_perso'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['rapidite_perso'];}
                        ?>" /></label>  

                        <label for="points_vie">Points de Vie
                            <select name="points_vie" size="1">
                                <option value="" selected></option>
                                <option value="20">Armure Légère</option>
                                <option value="25">Armure Intermédiaire</option>
                                <option value="30">Armure Lourde</option>
                            </select>
                        </label>

                        <label for="points_magie">Points de Magie
                        <input type="number" name="points_magie" id="points_magie" readonly="readonly" value="<?php
                        if (isset($_POST['points_magie'])){
                            echo $_POST['points_magie'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['points_magie'];}
                        ?>" /></label>                    

                    </fieldset>
                </div>

                <div id="group2">
                    <fieldset id="bloc3">
                        <legend>Ses Compétences</legend>

                        <label for="attaque_cac">Attaque Corps à Corps
                        <input type="text" name="attaque_cac" id="attaque_cac" readonly="readonly" value="<?php
                        if (isset($_POST['attaque_cac'])){
                            echo $_POST['attaque_cac'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['attaque_cac'];}
                        ?>" /></label>

                        <label for="attaque_dist">Attaque à Distance
                        <input type="text" name="attaque_dist" id="attaque_dist" readonly="readonly" value="<?php 
                        if (isset($_POST['attaque_dist'])){
                            echo $_POST['attaque_dist'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['attaque_dist'];}
                        ?>" /></label>

                        <label for="attaque_magie_cac">Attaque Magie Corps à Corps
                        <input type="text" name="attaque_magie_cac" id="attaque_magie_cac" readonly="readonly" value="<?php
                        if (isset($_POST['attaque_magie_cac'])){
                            echo $_POST['attaque_magie_cac'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['attaque_magie_cac'];}
                        ?>" /></label>

                        <label for="attaque_magie_dist">Attaque Magie à Distance
                        <input type="text" name="attaque_magie_dist" id="attaque_magie_dist" readonly="readonly" value="<?php
                        if (isset($_POST['attaque_magie_dist'])){
                            echo $_POST['attaque_magie_dist'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['attaque_magie_dist'];}
                        ?>" /></label>

                        <label for="degat_cac">Dégâts Corps à Corps
                        <input type="text" name="degat_cac" id="degat_cac" readonly="readonly" value="<?php
                        if (isset($_POST['degat_cac'])){
                            echo $_POST['degat_cac'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['degat_cac'];}
                        ?>" /></label>

                        <label for="degat_dist">Dégâts à Distance
                        <input type="text" name="degat_dist" id="degat_dist" readonly="readonly" value="<?php
                        if (isset($_POST['degat_dist'])){
                            echo $_POST['degat_dist'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['degat_dist'];}
                        ?>" /></label>

                        <label for="degat_magie">Dégâts Magie
                        <input type="text" name="degat_magie" id="degat_magie" readonly="readonly" value="<?php
                        if (isset($_POST['degat_magie'])){
                            echo $_POST['degat_magie'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['degat_magie'];}
                        ?>" /></label>

                        <label for="resist_physique">Résistance Physique
                        <input type="text" name="resist_physique" id="resist_physique" readonly="readonly" value="<?php
                        if (isset($_POST['resist_physique'])){
                            echo $_POST['resist_physique'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['resist_physique'];}
                        ?>" /></label>

                        <label for="resist_magie">Résistance à la Magie
                        <input type="text" name="resist_magie" id="resist_magie" readonly="readonly" value="<?php
                        if (isset($_POST['resist_magie'])){
                            echo $_POST['resist_magie'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['resist_magie'];}
                        ?>" /></label>

                        <label for="blocage">Blocage
                        <input type="text" name="blocage" id="blocage" readonly="readonly" value="<?php
                        if (isset($_POST['blocage'])){
                            echo $_POST['blocage'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['blocage'];}
                        ?>" /></label>

                        <label for="parade">Parade
                        <input type="text" name="parade" id="parade" readonly="readonly" value="<?php
                        if (isset($_POST['parade'])){
                            echo $_POST['parade'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['parade'];}
                        ?>" /></label>

                        <label for="esquive">Esquive
                        <input type="text" name="esquive" id="esquive" readonly="readonly" value="<?php
                        if (isset($_POST['esquive'])){
                            echo $_POST['esquive'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['esquive'];}
                        ?>" /></label>
                        
                    </fieldset>

                    <fieldset id="bloc4">
                        <legend>Sa Description</legend>

                        <label for="arme_cac">Armes Corps à Corps
                        <input type="text" name="arme_cac" id="arme_cac"  value="<?php
                        if (isset($_POST['arme_cac'])){
                            echo $_POST['arme_cac'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['arme_cac'];}
                        ?>" /></label>       

                        <label for="arme_dist">Armes à Distance
                        <input type="text" name="arme_dist" id="arme_dist"  value="<?php
                        if (isset($_POST['arme_dist'])){
                            echo $_POST['arme_dist'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['arme_dist'];}
                        ?>" /></label>  


                        <label for="inventaire">Inventaire Permanent
                        <textarea name="inventaire" id="inventaire" value="<?php
                        if (isset($_POST['inventaire'])){
                            echo $_POST['inventaire'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['inventaire'];}
                        ?>" ></textarea></label>

                        <label for="physique">Description Physique
                        <textarea name="physique" id="physique" value="<?php
                        if (isset($_POST['physique'])){
                            echo $_POST['physique'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['physique'];}
                        ?>" ></textarea></label>

                        <label for="physique">Divers
                        <textarea name="divers" id="divers" value="<?php
                        if (isset($_POST['physique'])){
                            echo $_POST['physique'];
                        }
                        if (!empty($_SESSION['id_fiche'])) {
                            echo $resultat['physique'];}
                        ?>" ></textarea></label>

                    </fieldset>
                </div>

                <div id="message_erreur">
                    <?php 
                    if (!empty($erreur)) echo "$message_champ";
                    if (isset($personnage)) echo "$personnage";
                    ?>
                </div>
                <div id="boutons">
                    <input type="hidden" id="valeur_fiche" name="valeur_fiche" value="<?php echo $_GET['id'] ?>" >
                    <img src="../img/reset.png" alt="RESET" id="refaire3" name="refaire" />
                    <button type="submit" value="submit" alt="envoyer" name="submit" id="validation"><img src="../img/valider.png" alt="envoyer" id="envoyer" /></button>
                </div>
            </form>  

        </main>

        <?php require 'footer.php'; ?>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="../js/calcul.js"></script>
        <script src="../js/bouton.js"></script>
    </body>
</html>
