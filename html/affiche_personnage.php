<?php 
session_start();

include('../php/connexionbdd.php');
include('../php/modif_personnage.php');
include('../php/affiche_personnage_simple.php');

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/style.css" />
        <title>Fiche Personnage</title>
        <!-- début Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="../img/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../img/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../img/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../img/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../img/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../img/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../img/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../img/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../img/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="../img/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../img/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon-16x16.png">
        <link rel="manifest" href="../img/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- Fin Favicon -->
    </head>
    <body>
        
        <?php require 'header.php'; ?>

        <main>    



            <?php echo '
            
            <div class="card mb-3">
                    <h3 class="card-header">' .$resultat['nom_perso']. '&nbsp' .$resultat['prenom_perso']. '</h3>
                    <div class="card-body">
                        <h5 class="card-title">' .$resultat['age_perso'].' ans - '.$resultat['metier']. '</h5>
                        <h6 class="card-subtitle text-muted">'.$resultat['nom_guilde']. ' - ' .$resultat['role_guilde']. '</h6>
                    </div>
                    <img style=" height: 15rem; width: 10rem; display: block;" src="'.$dir.$resultat['photo'].'" alt="Card image" class="rounded-circle">
                    <div class="card-body">
                        <ul class="list-group list-group-horizontal-xl">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Points de vie :
                            <span class="badge badge-primary badge-pill">' .$resultat['points_vie'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Points de magie :
                            <span class="badge badge-primary badge-pill">' .$resultat['points_magie'].'</span>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-horizontal-xl">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            FORCE :
                            <span class="badge badge-primary badge-pill">' .$resultat['force_perso'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            CONSTITUTION :
                            <span class="badge badge-primary badge-pill">' .$resultat['constitution_perso'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            DEXTERITE :
                            <span class="badge badge-primary badge-pill">' .$resultat['dexterite_perso'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            INTELLIGENCE :
                            <span class="badge badge-primary badge-pill">' .$resultat['intelligence_perso'].'</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-horizontal-xl">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            CHARISME :
                            <span class="badge badge-primary badge-pill">' .$resultat['charisme_perso'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            PERCEPTION :
                            <span class="badge badge-primary badge-pill">' .$resultat['perception_perso'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            VOLONTE :
                            <span class="badge badge-primary badge-pill">' .$resultat['volonte_perso'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            RAPIDITE :
                            <span class="badge badge-primary badge-pill">' .$resultat['rapidite_perso'].'</span>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-horizontal-xl">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Attaque Corps à Corps :
                            <span class="badge badge-primary badge-pill">' .$resultat['attaque_cac'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Attaque à Distance :
                            <span class="badge badge-primary badge-pill">' .$resultat['attaque_dist'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Attaque Magie Corps à Corps :
                            <span class="badge badge-primary badge-pill">' .$resultat['attaque_magie_cac'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Attaque Magie à Distance :
                            <span class="badge badge-primary badge-pill">' .$resultat['attaque_magie_dist'].'</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-horizontal-xl">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Dégâts au Corps à Corps :
                            <span class="badge badge-primary badge-pill">' .$resultat['degat_cac'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Dégâts à Distance :
                            <span class="badge badge-primary badge-pill">' .$resultat['degat_dist'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Dégâts Magie :
                            <span class="badge badge-primary badge-pill">' .$resultat['degat_magie'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Résistance Physique :
                            <span class="badge badge-primary badge-pill">' .$resultat['resist_physique'].'</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-horizontal-xl">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Résistance Magie :
                            <span class="badge badge-primary badge-pill">' .$resultat['resist_magie'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Blocage :
                            <span class="badge badge-primary badge-pill">' .$resultat['blocage'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Parade :
                            <span class="badge badge-primary badge-pill">' .$resultat['parade'].'</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Esquive :
                            <span class="badge badge-primary badge-pill">' .$resultat['esquive'].'</span>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Attaque Corps à Corps :
                            <p>' .$resultat['arme_cac'].'</p>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Attaque à Distance :
                            <p>' .$resultat['arme_dist'].'</p>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Attaque Magie Corps à Corps :
                            <p>' .$resultat['inventaire'].'</p>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Attaque Magie à Distance :
                            <p>' .$resultat['physique'].'</p>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                            Divers :
                            <p>' .$resultat['divers'].'</p>
                            </li>
                        </ul>
                    </div>
                    <div class="btn-group" role="group">
                    <a href="../html/personnage.php?id=' .$_GET['id']. '" class="card-link"><button type="button" class="btn btn-light">Modifier</button></a>
                    <a href="#" class="card-link"><button type="button" class="btn btn-danger">Supprimer</button></a>
                    </div>
                    <div class="card-footer text-muted">
                        <p>Date création : '.date('d/m/Y', strtotime($resultat['date_creation'])).'</p>
                    </div>
                </div>'

                ?>
            
        </main>

        <?php require 'footer.php'; ?>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="../js/calcul.js"></script>
        <script src="../js/bouton.js"></script>
    </body>
</html>
