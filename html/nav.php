
<nav class="navbar navbar-expand-lg navbar-light bg-light">
 
    <?php 
        
        // if(isset($_SESSION['role_joueur'])) {
        //     $_SESSION['role_joueur'] = "" ;
        // }

        // var_dump($_SESSION['role_joueur']);
        
        if (isset($_SESSION['role_joueur'])) {

            echo "
            <div class=\"collapse navbar-collapse\" id=\"navbarNav\">
                <ul class=\"navbar-nav\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"inscription.php\">S'inscrire</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"connexion.php\">Se Connecter</a>
                    </li>
                </ul>
            </div> ";
            

            if ($_SESSION['role_joueur']== "inscrit") {
                echo "
                <div class=\"collapse navbar-collapse\" id=\"navbarNav\">
                    <ul class=\"navbar-nav\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"accueil.php\">Accueil</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"personnage.php\">Nouveau Personnage</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"../php/deconnexion.php\">Deconnexion</a>
                        </li>
                    </ul>
                </div> ";       
            };

            if ($_SESSION['role_joueur']== "chef_guilde") {
                echo "
                <div class=\"collapse navbar-collapse\" id=\"navbarNav\">
                    <ul class=\"navbar-nav\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"accueil.php\">Accueil</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"personnage.php\">Nouveau Personnage</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"../php/deconnexion.php\">Deconnexion</a>
                        </li>
                    </ul>
                </div> ";     
            };
            
            if ($_SESSION['role_joueur']== "admin") {
                echo "
                <div class=\"collapse navbar-collapse\" id=\"navbarNav\">
                    <ul class=\"navbar-nav\">
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"accueil.php\">Accueil</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"personnage.php\">Nouveau Personnage</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"ajout_classe.php\">Nouvelle Classe</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"ajout_guilde.php\">Nouvelle Guilde</a>
                        </li>
                        <li class=\"nav-item\">
                        <a class=\"nav-link\" href=\"../php/deconnexion.php\">Deconnexion</a>
                        </li>
                    </ul>
                </div> ";     
            };
        };
    ?>

</nav>

