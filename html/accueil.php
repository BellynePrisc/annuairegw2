<?php 

session_start();


if(!isset($_SESSION['role_joueur']) || $_SESSION['role_joueur'] != 'admin') {
    echo "Vous n'avez pas accès à cette partie du site ! Aurevoir.";
    exit();
}
require '../php/affiche_index_personnage.php'

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/style.css" />
        <title>Accueil</title>
    </head>
    <body>

        <?php require 'header.php'; ?>

        <main>

            <div class="jumbotron"> 
                <p>Vos Fiches</p>


                <div class="miniatures">

                    <?php 
                        if (!empty($reponse)) {
                            foreach ($reponse as $index){
                                echo '<div class="card" style="width: 18rem align-text: center;">
                                <div class="card-body">
                                <h5 class="card-title">' .$index['nom_perso']. '&nbsp' .$index['prenom_perso']. '</h5>
                                <p class="card-text">Classe : ' .$index['classe']. '</p>
                                <p>Guilde : ' .$index['nom_guilde']. '</a>
                                <p><a href="affiche_personnage.php?id=' .$index['id'].'"><button type="button" class="btn btn-light">Voir</button></a></p>

                            </div>';
                            };
                        } 
                        else {
                            echo 'Pas d\'enregistrement trouvé !';
                        }
                    ?>   

                </div>

            </div>
        
        </main>

        <?php require 'footer.php'; ?>
            
    </body>

</html>