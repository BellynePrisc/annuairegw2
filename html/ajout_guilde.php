<?php 
session_start();
if(!isset($_SESSION['role_joueur']) || $_SESSION['role_joueur'] != 'admin') {
    echo "Vous n'avez pas accès à cette partie du site ! Aurevoir.";
    exit();
}
include ('../php/connexionbdd.php');
require('../php/ajout_guilde_post.php');
if (!empty($guilde_ok)) header("Refresh: 3;url=accueil.php");
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/style.css" />
        <title>Nouvelle Guilde</title>
        <!-- début Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="../img/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../img/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../img/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../img/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../img/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../img/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../img/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../img/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../img/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="../img/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../img/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../img/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../img/favicon-16x16.png">
        <link rel="manifest" href="../img/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="img/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!-- Fin Favicon -->
    </head>
    <body>
        
        <?php require 'header.php'; ?>

        <main>     

            <form id="monformulaire4" name="monformulaire4" action="ajout_guilde.php" method="post" enctype="multipart/form-data">
                <div id="group1">
                    <fieldset id="bloc1">
                        <legend>Nouvelle Guilde</legend>

                        <label for="nom_guilde">Nom Guilde
                        <input type="text" name="nom_guilde" id="nom_guilde" value="<?php if (isset($_POST['nom_guilde'])){echo $_POST['nom_guilde'];} ?>" /></label>

                        <label for="embleme_guilde">Emblême (pas d'espace ni ponctuation)
                        <input type="file" name="embleme_guilde" id="embleme_guilde" accept=".jpg, .jpeg, .png" value="" /></label>
                    </fieldset>
                </div>
                <div id="message_erreur">
                    <?php 
                        if (!empty($upload_ok)) echo "$upload_ok";
                        if (!empty($upload_fail)) echo "$upload_fail";
                        if (!empty($erreur_guilde)) echo "$erreur_guilde";
                        if (!empty($guilde_ok)) echo "$guilde_ok";
                    ?>
                </div>
                <div id="boutons">
                    <img src="../img/reset.png" alt="RESET" id="refaire3" name="refaire" />
                    <button type="submit" value="submit" alt="envoyer" name="submit" id="validation"><img src="../img/valider.png" alt="envoyer" id="envoyer" /></button>  
                </div>
            </form> 

        </main>

        <?php require 'footer.php'; ?>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
        <script src="../js/bouton.js"></script>
    </body>

</html>